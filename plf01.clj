(ns plf01.core)


(defn función-Compara-1
  [a b c d]
  (> (+ a b) (- c d)))

(defn función-Compara-2
  [a b]
  (> a b))

(defn función-Compara-3
  [a b c d]
  (> (* a b) (+ c d)))

(función-Compara-1 2 1 4 2)
(función-Compara-2 2 1)
(función-Compara-3 2 1 4 2)



(defn función-ComparaMayorIgual-1
  [a b c d]
  (>= (+ a b) (- c d)))

(defn función-ComparaMayorIgual-2
  [a b]
  (>= a b))

(defn función-ComparaMayorIgual-3
  [a b c d]
  (>= (* a b) (+ c d)))

(función-ComparaMayorIgual-1 2 1 4 2)
(función-ComparaMayorIgual-2 2 2)
(función-ComparaMayorIgual-3 3 5 4 2)



(defn función-ComparaMenorIgual-1
  [a b c d]
  (<= (+ a b) (- c d)))

(defn función-ComparaMenorIgual-2
  [a b]
  (<= a b))

(defn función-ComparaMenorIgual-3
  [a b c d]
  (<= (* a b) (+ c d)))

(función-ComparaMenorIgual-1 2 1 4 2)
(función-ComparaMenorIgual-2 2 2)
(función-ComparaMenorIgual-3 3 5 4 2)


(defn función-ComparaMenor-1
  [a b c d]
  (< (+ a b) (- c d)))

(defn función-ComparaMenor-2
  [a b c]
  (< a (/ b c)))

(defn función-ComparaMenor-3
  [a b c d]
  (< (* a b) (* c d)))

(función-ComparaMenor-1 2 1 4 2)
(función-ComparaMenor-2 2 6 3)
(función-ComparaMenor-3 3 5 4 2)

(defn función-Get-1
  [a b]
  (get a b))

(defn función-Get-2
  [a b]
  (get a b))

(defn función-Get-3
  [a b]
  (get a b))

(función-Get-1 [1 2 3 4 5] 2)
(función-Get-2 [1 2 3 4 5] 12)
(función-Get-3 ["a" "b" "c" "d" "e"] 2)

(defn funcion-GetIn-1
  [a]
  (let [InfoPoblacion {:poblaciones {:poblacionCondiciones {:PoblaciónEnEl2000 3438765
                                                            :PoblaciónEnEl2005 3506821
                                                            :PoblaciónEnEl2010 3801962
                                                            :PoblaciónEnEl2015 3976297}}}]
    (get-in InfoPoblacion a))
  )

(defn funcion-GetIn-2
  [a b]
  
    (get-in a b))

(defn funcion-GetIn-3
  [a]
  (let [InfoPoblacion {:poblaciones {:poblacionCondiciones {:PoblaciónEnEl2000 3438765
                                                            :PoblaciónEnEl2005 3506821
                                                            :PoblaciónEnEl2010 3801962
                                                            :PoblaciónEnEl2015 3976297}}}]
    (get-in InfoPoblacion a)))

(funcion-GetIn-1 [:poblaciones :poblacionCondiciones :PoblaciónEnEl2000])
(funcion-GetIn-2 {:poblaciones {:poblacionCondiciones {:PoblaciónEnEl2000 3438765}}} [:poblaciones :poblacionCondiciones :PoblaciónEnEl2000])
(funcion-GetIn-3 [:poblaciones :poblacionCondiciones :PoblaciónEnEl2015])


(defn función-Nil-1
  [a]
  (nil? a))

(defn función-Nil-2
  [a]
  (nil? a))

(defn función-Nil-3
  [a]
  (nil? a))

(función-Nil-1 nil)
(función-Nil-2 "foo")
(función-Nil-3 true)

(defn función-ComparaIgual-1
  [a b c d]
  (== (+ a b) (* c d)))

(defn función-ComparaIgual-2
  [a b c d]
  (== (* a b) (/ c d)))

(defn función-ComparaIgual-3
  [a b c d]
  (== (* a b) (* c d)))

(función-ComparaIgual-1 2 1 4 2)
(función-ComparaIgual-2 2 3 6 1)
(función-ComparaIgual-3 3 5 4 2)

(defn función-False-1
  [a]
  (false? a))

(defn función-False-2
  [a ]
  (false? a))

(defn función-False-3
  [a]
  (false? a))

(función-False-1 false)
(función-False-2 "foo")
(función-False-3 true)


(defn función-Negativo-1
  [a b c d]
  (neg? (- (/ a b) (+ c d))))

(defn función-Negativo-2
  [a b]
  (neg? (- a b)))

(defn función-Negativo-3
  [a b c d]
  (neg? (- (* a b) (* c d))))

(función-Negativo-1 -2 1 4 2)
(función-Negativo-2 2 2)
(función-Negativo-3 3 -5 4 2)


(defn función-Positivo-1
  [a b c d]
  (pos? (- (/ a b) (+ c d))))

(defn función-Positivo-2
  [a b]
  (pos? (- a b)))

(defn función-Positivo-3
  [a b c d]
  (pos? (- (* a b) (* c d))))

(función-Positivo-1 -2 1 4 2)
(función-Positivo-2 2 2)
(función-Positivo-3 3 -5 4 2)


(defn función-Cadean-1
  [a b]
  (str a b))

(defn función-Cadean-2
  [a b]
  (str a  100 b))

(defn función-Cadean-3
  [a b]
  (pr-str (take a (range b))))

(función-Cadean-1 "Esta es una cadena" " Este es el complemneto de la cadena")
(función-Cadean-2 "Esta es una cadena con el numero: " " Enmedio")
(función-Cadean-3 5 10)


(defn función-True-1
  [a]
  (true? a))

(defn función-True-2
  [a]
  (true? a))

(defn función-True-3
  [a]
  (true? a))

(función-True-1 false)
(función-True-2 "foo")
(función-True-3 true)


(defn función-Vals-1
  [a]
  (vals a))

(defn función-Vals-2
  [a]
  (vals a))

(defn función-Vals-3
  [a]
  (vals a))

(función-Vals-1 {1 2 3 4})
(función-Vals-2 {})
(función-Vals-3 {1 2 3 4 5 6 7 8})


(defn función-Zero-1
  [a b c d]
  (zero? (- (/ a b)(+ c d))))

(defn función-Zero-2
  [a b]
  (zero? (- a b)))

(defn función-Zero-3
  [a b c d]
  (zero? (- (* a b) (* c d))))

(función-Zero-1 2 1 4 2)
(función-Zero-2 2 2)
(función-Zero-3 3 5 4 2)